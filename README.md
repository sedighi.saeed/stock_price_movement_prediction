## Project title
Stock Price Movement Prediction

## Motivation
This project is motivated by Financial and Macroeconomics Connectedness created by Diebold and Ylimaz. Predict stock movement based on the connections between stock, currency, debt market.

## Flow
![alt text](https://gitlab.com/YCChen_Victor/stock_price_movement_prediction/blob/master/Untitled%20Diagram.png)

## Features
Currenctly around 80% prediction accuracy (^TWII)

## Installation
1. Install Python 3
2. pip install -r requirements.txt

## How to use?
1. Put a folder with multiple Panel data into docs folder, all
2. In terminal run python full_predict_procedure_CNN_RNN

## Credits
http://financialconnectedness.org/

## Future development
1. ConvLSTM
2. risk classification

## License
MIT License
